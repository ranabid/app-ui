import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorHandlerComponent } from './error-handler/error-handler.component';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
	{path: '', redirectTo: '/home', pathMatch: 'full'},
	{path: 'home', component: DashboardComponent, pathMatch: 'full'},
  { path: 'error', component: ErrorHandlerComponent, pathMatch: 'full'}
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
  	exports: [ RouterModule ]
})
export class AppRoutingModule {}