import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StartupData } from './startup-data';

@Injectable()
export class StartupService {
	private _startupData: StartupData;
	private baseUri: string = 'http://localhost:8080/webapp/';
	
	constructor(private http: HttpClient) { }

	load(): Promise<any|StartupData> {
		this._startupData = null;
		return this.http
			.get<any>(this.baseUri + 'service/api/data')
			.toPromise()
            .then((data: any) => this._startupData = data)
            .catch((err: any) => Promise.resolve());
	}

	get startupData(): StartupData{
        return this._startupData;
    }

}
