import { Component, OnInit } from '@angular/core';
import { SkyeService } from '../skye.service';
import { User } from '../app.common-object';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
	user: User;
  constructor(private skyeService: SkyeService) { }

  ngOnInit() {
  	this.skyeService.getUser()
  		.subscribe(user => this.user = user);

  }

}