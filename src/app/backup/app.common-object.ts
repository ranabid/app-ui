export class Endpoint {
	skyeApi: string;
	userApi: string;
	videosApi: string;
	playlistsApi: string;

	constructor() {
		this.skyeApi = 'http://localhost:8080/api/skye';
		this.userApi = 'http://localhost:8080/api/skye/user';
		this.videosApi = 'http://localhost:8080/api/skye/videos';
		this.playlistsApi = 'http://localhost:8080/api/skye/playlists';
	}
}

export class User {
	id: number;
	name: string;
}
export class Video {
	id: number;
	name: string;
}
export class Playlist {
	id: number;
	name: string;
	videos: Video[];
	createdDate: string;
	createdBy: User;
}
export class SkyeInitData {
	user: User;
	videos: Video[];
	playlists: Playlist[];
}

