import { TestBed, inject } from '@angular/core/testing';

import { StartupDataService } from './startup-data.service';

describe('StartupDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StartupDataService]
    });
  });

  it('should be created', inject([StartupDataService], (service: StartupDataService) => {
    expect(service).toBeTruthy();
  }));
});
