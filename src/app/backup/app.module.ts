import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { SkyeService } from './skye.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { VideosListComponent } from './videos-list/videos-list.component';
import { HeaderComponent } from './header/header.component';
import { StartupDataService } from './startup-data.service';
import { VideoComponent } from './video/video.component';
import { VideoService } from './video.service';


export function init_app(startupDataService: StartupDataService) {
    return () => startupDataService.loadData();
}

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    VideosListComponent,
    HeaderComponent,
    VideoComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER, 
      useFactory: init_app,
      multi: true,
      deps: [ StartupDataService ]
    },
    SkyeService,
    StartupDataService,
    VideoService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
