import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User, Video, Playlist } from './app.common-object';
import { StartupDataService } from './startup-data.service';

@Injectable()
export class SkyeService {
	
	private httpOptions = {
  		headers: new HttpHeaders({ 'Content-Type': 'application/json' })
	};

  	constructor(private http: HttpClient, private startupDataService: StartupDataService) { }

  	getUser (): Observable<User> {
  		return this.startupDataService.getStartupData().user;
	}

	getVideos(): Observable<Video[]> {
		return this.startupDataService.getStartupData().videos;
	}

	getPlaylists(userId: number): Observable<Playlist[]> {
		return this.startupDataService.getStartupData().playlists;
	}
/*
	getVideo(id: number): Observable<Video> {
		const videoUrl = `${this.videosApi}/${id}`;
		return this.http.get<Video>(videoUrl);
	}
	
	updatePlaylist (playlist: Playlist): Observable<any> {
  		return this.http.put(this.playlistsApi, playlist, this.httpOptions);
	}

	addPlaylist(playlist: Playlist): Observable<Playlist>{

		return this.http.post<Playlist>(this.playlistsApi, playlist, this.httpOptions);
	}

	deletePlaylist(playlist: Playlist | number): Observable<Playlist> {
		let id = typeof playlist === 'number' ? playlist : playlist.id;
		let url = `${this.playlistsApi}/${id}`;

		return this.http.delete<Playlist>(url, this.httpOptions);
	}
*/

}
