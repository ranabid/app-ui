import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SidebarComponent } from './sidebar/sidebar.component';

const routes: Routes = [
	{ path: '', redirectTo: '/skye', pathMatch: 'full' },
  	{ path: 'skye', component: SidebarComponent }
];

@NgModule({
  imports: [
  	RouterModule.forRoot(routes),
    CommonModule
  ],
  declarations: [], 
  exports: [RouterModule ]
})
export class AppRoutingModule { }
