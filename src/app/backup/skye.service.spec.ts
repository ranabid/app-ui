import { TestBed, inject } from '@angular/core/testing';

import { SkyeService } from './skye.service';

describe('SkyeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SkyeService]
    });
  });

  it('should be created', inject([SkyeService], (service: SkyeService) => {
    expect(service).toBeTruthy();
  }));
});
