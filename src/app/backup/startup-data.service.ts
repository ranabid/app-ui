import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class StartupDataService {
	private _startupData: any;
  	
  	constructor(private http: HttpClient) { }
  	
  	loadData(): Promise<any> {

  		this._startupData = null;
    	const promise = this.http.get("http://localhost:8080/api/skye")
    		.toPromise()
    		.then((data: any)=>this._startupData = data)
    		.catch((err: any) => Promise.resolve());
    	return promise;
  	}

  	getStartupData(): any {
        return this._startupData;
    }
}
