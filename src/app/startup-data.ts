export class StartupData {
	user: User;
	videos: Video[];
	playlists: Playlist[];	
}

export class User {
	id: number;
	name: string;
}

export class Video {
	id: number;
	videoName: string;
}

export class Playlist {
	id: number;
	playlistName;
	videos: Video[];
}


