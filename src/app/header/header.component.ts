import { Component, OnInit, Input } from '@angular/core';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import { User } from '../startup-data';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'], 
  providers: [NgbDropdownConfig]
})
export class HeaderComponent implements OnInit {
  @Input() title: string;
  @Input("user") user: User; 
  isUserValid: boolean;
  constructor(config: NgbDropdownConfig) {
  	config.placement = 'bottom-right';
    config.autoClose = true;
  }

  ngOnInit() {
    this.isUserValid = true;
  }

}
