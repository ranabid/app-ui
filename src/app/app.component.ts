import { Component, OnInit } from '@angular/core';
import { StartupService } from './startup.service';
import { StartupData, User } from './startup-data';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	title: string;
	startupData: StartupData;
	user: User;
	constructor(private startup: StartupService, private router: Router) {}

  	ngOnInit() {
  		this.title = "MyAPP";
       	if (!this.startup.startupData) {
       		this.user = null;
           this.router.navigate(['error'], { replaceUrl: true });
        }
        else {
            this.startupData = this.startup.startupData;
            this.user = this.startupData.user;
        }
    }
}
